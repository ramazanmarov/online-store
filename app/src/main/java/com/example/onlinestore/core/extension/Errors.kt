package com.example.onlinestore.core.extension

class ServerError(override val message: String?): Exception(message)
class NoDigitalSignatureException(override val message: String): Exception(message)