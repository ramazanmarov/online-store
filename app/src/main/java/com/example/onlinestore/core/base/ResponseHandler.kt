package com.example.core.base

import com.example.onlinestore.core.base.ServerErrorHandler
import io.reactivex.observers.DisposableObserver

open class ResponseHandler<T : Any> (
    val errorHandler: ServerErrorHandler,
    val onSuccess: (T) -> Unit = {},
    val onFailure: ((Throwable) -> Unit)? = null
    ) : DisposableObserver<T>() {
        override fun onComplete() {
        }

        override fun onNext(t: T) = onSuccess(t)

        override fun onError(e: Throwable) {
            if (onFailure != null) {
                onFailure.invoke(e); return
            }
            errorHandler.handle(e)
        }
}