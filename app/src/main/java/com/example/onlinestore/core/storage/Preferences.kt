package com.example.onlinestore.core.storage

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.example.onlinestore.core.extension.empty

class Preferences(val context: Context) {

    private val sharedPreferences: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(context)
    }

    var token: String
        get() {
            return getStringByKey(PrefKeys.SESSION_ID)
        }
        set(value) {
            setStringByKey(PrefKeys.SESSION_ID, value)
        }

    var isNotVirgin: Boolean
        get() {
            return getBooleanByKey(PrefKeys.FIRST_LAUNCH)
        }
        set(value) {
            setBooleanByKey(PrefKeys.FIRST_LAUNCH, value)
        }

    private fun getBooleanByKey(key: String): Boolean = sharedPreferences.getBoolean(key, false)
    private fun setBooleanByKey(key: String, value: Boolean){
        sharedPreferences
            .edit()
            .putBoolean(key, value)
            .apply()
    }

    private fun getStringByKey(key: String): String =
        sharedPreferences.getString(key, String.empty) ?: String.empty

    private fun setStringByKey(key: String, value: String) {
        sharedPreferences
            .edit()
            .putString(key, value)
            .apply()
    }

    fun clearUserData() {
        token = ""
    }

    object PrefKeys {
        const val SESSION_ID = "SESSION_ID"
        const val FIRST_LAUNCH = "FIRST_LAUNCH"

    }
}