package com.example.onlinestore.core.extension

import java.math.BigInteger
import java.security.MessageDigest

val String.Companion.empty: String
    get() = ""

fun String.asMd5(): String {
    val messageDigest = MessageDigest.getInstance("MD5")
    val digest = messageDigest.digest(this.toByteArray())
    return BigInteger(1, digest).toString(16)
}

