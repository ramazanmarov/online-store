package com.example.onlinestore.core.custom

import android.content.Context
import android.os.Build
import android.preference.PreferenceManager
import java.util.Locale

object LocaleHelper {
    private val SELECTED_LANGUAGE = "Locale.Helper.Selected.Language"
    const val RU = "ru"
    const val KG = "kg"

    fun onAttach (context: Context): Context {
        val lang = getPersistedData(context, getLanguage(context))
        return setLocale(context, lang)
    }

    fun getPersistedData(context: Context, defaultLanguage: String): String?{
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString(SELECTED_LANGUAGE,defaultLanguage)
    }

    fun getLanguage(context: Context): String{
        return getPersistedData(context, RU)?: RU
    }

    fun setLocale(context: Context, language: String?): Context {
        persist(context,language)

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            updateResourses(context, language)
        }else updateResoursesLegacy(context, language)
    }

    private fun persist (context: Context, language: String?){
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = preferences.edit()

        editor.putString(SELECTED_LANGUAGE, language)
        editor.apply()
    }

    private fun updateResourses(context: Context, language: String?): Context {
        val locale = Locale(language)
        Locale.setDefault(locale)

        val configuration = context.resources.configuration
        configuration.setLocale(locale)
        configuration.setLayoutDirection(locale)

        return context.createConfigurationContext(configuration)
    }

    private fun updateResoursesLegacy(context: Context, language: String?): Context {
        val locale = Locale(language)
        Locale.setDefault(locale)

        val resources = context.resources
        val configuration = resources.configuration

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            configuration.setLayoutDirection(locale)
            resources.configuration.setLocale(locale)
        }

        return context
    }

}