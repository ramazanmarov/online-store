package com.example.onlinestore.di

import android.content.Context
import com.example.onlinestore.core.base.ServerErrorHandler
import com.example.onlinestore.core.storage.Preferences
import com.example.onlinestore.network.base.provideOkHttp
import com.example.onlinestore.network.base.provideRetrofit
import com.example.onlinestore.network.base.provideSession
import com.example.onlinestore.repository.RepositoryApi
import com.example.onlinestore.ui.main.MainVM
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    viewModel { MainVM(get()) }
    factory { RepositoryApi(get()) }
//    factory { RepositoryDB(get()) }
//    single { provideDataBase(get())}
//    single { get<OnlineStoreDB>().dao()}
}

val networkModule = module{
    single { provideOkHttp(get()) }
    single { provideRetrofit(get(),   "https://fakestoreapi.com") }
    factory { provideSession(get()) }
}

val baseModule = module {
    single { Preferences(get()) }
    single { androidContext().resources }
    factory { (context: Context) -> ServerErrorHandler(context) }
}

//private fun provideDataBase(context: Context){
//    Room.databaseBuilder(context, OnlineStoreDB::class.java, "OnlineStoreDB").build()
//}

