package com.example.onlinestore.db.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.onlinestore.network.models.ProductModel
import io.reactivex.Flowable

@Dao
interface Dao {

    @Query("SELECT * FROM products_table WHERE title LIKE :searchQuery " +
                "OR description LIKE :searchQuery OR category LIKE :searchQuery"
    )
    fun searchDatabase(searchQuery: String): LiveData<MutableList<PModel>>

    @Query("SELECT * FROM categories")
    fun getCategoriesListFromDB(): List<Categories>

    @Query("SELECT * FROM categories")
    fun getCategoriesFromDB(): Flowable<List<Categories>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addCategoriesInDB(category: Categories)

    @Delete
    fun deleteCategories(categories: Categories)

    @Query("SELECT * FROM products_table WHERE category LIKE :category")
    fun getProductsFromDB(category: String): Flowable<MutableList<PModel>>

    @Query("SELECT * FROM products_table WHERE category LIKE :category")
    fun getProductsListFromDB(category: String): MutableList<PModel>

//    order by popularity DESC

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addProductsInDB(product: PModel)

    @Delete
    fun deleteProducts(products: PModel)

    @Query("SELECT * FROM products_table WHERE id LIKE :id")
    fun getProductDetailsFromDB(id: Int): Flowable<MutableList<PModel>>

    @Query("SELECT * FROM products_table WHERE id LIKE :id")
    fun getProductDetailsListFromDB(id: Int): MutableList<PModel>

    @Delete
    fun deleteAll(categories: Categories, products: PModel)
}

