package com.example.onlinestore.db.base

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation
import java.io.Serializable

@Entity(tableName = "products_table")
data class PModel(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val title: String,
    val price: Double,
    val description: String,
    val category: String,
    val image: String
//    @Embedded val rating: Rating?

) : Serializable

data class Rating(
    val rate: Double,
    val count: Int
):Serializable

@Entity(tableName = "categories")
data class Categories(
    @PrimaryKey()
    val id: String,
    val name: String
):Serializable
