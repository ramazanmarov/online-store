package com.example.onlinestore.db.base

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [PModel::class, Categories::class], version = 2, exportSchema = false)
abstract class OnlineStoreDB : RoomDatabase() {
    abstract fun dao(): Dao

    companion object {
        var INSTANCE: OnlineStoreDB? = null

         fun provideDataBase(context: Context): OnlineStoreDB {
            if (INSTANCE == null) {
                synchronized(this) {
                    INSTANCE =
                        Room.databaseBuilder(context, OnlineStoreDB::class.java, "OnlineStoreDB")
                            .allowMainThreadQueries()
                            .build()
                }
            }
            return INSTANCE!!
        }
    }
}
