package com.example.onlinestore.network.base

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class Schedulers {
    fun ui() = AndroidSchedulers.mainThread()
    fun io() = Schedulers.io()
}