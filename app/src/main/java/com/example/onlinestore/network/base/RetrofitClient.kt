package com.example.onlinestore.network.base

import androidx.multidex.BuildConfig
import com.google.gson.GsonBuilder
import com.example.onlinestore.core.storage.Preferences
import com.example.onlinestore.network.api.AppApi
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

fun provideOkHttp(preferences: Preferences): OkHttpClient {
    val builder = OkHttpClient.Builder()
    if (BuildConfig.DEBUG) {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        builder.readTimeout(60, TimeUnit.SECONDS)
        builder.writeTimeout(60, TimeUnit.SECONDS)
        builder.addInterceptor(loggingInterceptor)
    }
    builder.addInterceptor(getHeaderInterceptor(preferences))
    return builder.build()
}

private fun getHeaderInterceptor(preferences: Preferences): Interceptor {
    return Interceptor { chain ->
        chain.proceed(
            chain
                .request()
                .newBuilder()
                .build()
        )
    }
}

fun provideRetrofit(okHttpClient: OkHttpClient, url: String): Retrofit {
    return Retrofit.Builder()
        .client(okHttpClient)
        .baseUrl(url)
        .addConverterFactory(GsonConverterFactory.create())
        .addConverterFactory(ScalarsConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(NullConverterFactory())
        .addConverterFactory(
            ResponseConvertorFactory(
                GsonBuilder().enableComplexMapKeySerialization()
                    .create()
            )
        )
        .build()
}

fun provideSession(retrofit: Retrofit): AppApi = retrofit.create(AppApi::class.java)
