package com.example.onlinestore.network.api

import com.example.onlinestore.db.base.PModel
import com.example.onlinestore.network.models.ProductModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface AppApi {

    @GET("/products/categories")
    fun getCategories(): Observable<List<String>>

    @GET("/products/category/{categoryId}")
    fun getProduct(
        @Path("categoryId") categories: String): Observable<List<PModel>>

    @GET("/products/{id}")
    fun getProductDetails(
        @Path("id") id: Int): Observable<ProductModel>

    @GET("/products/category/{categoryId}?sort=asc")
    fun getSortAsc(
        @Path("categoryId") categories: String): Observable<List<ProductModel>>

    @GET("/products/category/{categoryId}?sort=desc")
    fun getSortDesc(
        @Path("categoryId") categories: String): Observable<List<ProductModel>>

}