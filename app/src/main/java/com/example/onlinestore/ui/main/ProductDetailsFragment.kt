package com.example.onlinestore.ui.main

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.net.toUri
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.onlinestore.core.base.BaseFragment
import com.example.onlinestore.core.base.ServerErrorHandler
import com.example.onlinestore.databinding.FragmentMainBinding
import com.example.onlinestore.databinding.FragmentProductDetailsBinding
import com.example.onlinestore.network.base.InternetConnection
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProductDetailsFragment : BaseFragment<FragmentProductDetailsBinding>() {
    override fun getViewBinding() = FragmentProductDetailsBinding.inflate(layoutInflater)
    private val viewModel by viewModel<MainVM>()
    private val args: ProductDetailsFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        initListeners()
        subLiveData()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    private fun setupView() = with(binding) {
        viewModel.context = requireContext()
        viewModel.exaptionHandler = ServerErrorHandler(requireContext())
        getProductDetails()
    }

    @SuppressLint("SetTextI18n")
    private fun subLiveData() = with(binding) {
        viewModel.productDetails.observe(viewLifecycleOwner) {
            Glide.with(requireContext()).load(it.image).into(vpImage)
            tvProductTitle.text = it.title
            tvProductDesc.text = it.description
            tvProductPrice.text = "$" + it.price.toString()
            tvProductCategory.text = it.category
            tvRating.text = it.rating.rate.toString()
            tvCount.text = it.rating.count.toString()
            hideDialog()
        }
    }

    private fun initListeners() {
        binding.btnBack.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun getProductDetails()= with(binding) {
        if (InternetConnection.isNetAvailable(requireContext())) {
            showDialog()
            viewModel.getProductDetails(args.pModel.id)
        } else {
            tvRating.isVisible = false
            tvCount.isVisible = false
            if (!viewModel.isProductDetailsIsEmpty(args.pModel.id)) {
                Glide.with(requireContext()).load(args.pModel.image).into(vpImage)
                tvProductTitle.text = args.pModel.title
                tvProductDesc.text = args.pModel.description
                tvProductPrice.text = "$" + args.pModel.price.toString()
                tvProductCategory.text = args.pModel.category
                hideDialog()
            }
        }
    }

    override fun onBackPressed() {
        findNavController().popBackStack()
    }
}