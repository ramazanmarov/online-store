package com.example.onlinestore.ui.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.example.onlinestore.R
import com.example.onlinestore.core.custom.FullScreenProgressDialog
import com.example.onlinestore.core.storage.Preferences
import com.example.onlinestore.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var alertDialog: FullScreenProgressDialog
    private lateinit var prefs: Preferences
    private lateinit var navHostFragment: NavHostFragment
    private lateinit var navHostController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        prefs = Preferences(this)
        initNavGraph()
    }

    fun showDialog() {
        alertDialog = FullScreenProgressDialog()
        alertDialog.show(supportFragmentManager, "popUp")
    }

    fun hideDialog() {
        alertDialog.dismiss()
    }

    @SuppressLint("RestrictedApi")
    private fun initNavGraph() {
        navHostFragment = supportFragmentManager.findFragmentById(R.id.fragmentContainerView)
                as NavHostFragment
        navHostController = navHostFragment.navController
        binding.bottomMenu.setupWithNavController(navHostController)
        navHostController.enableOnBackPressed(true)

        navHostController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.fragment_main,
                R.id.fragment_basket,
                R.id.fragment_profile,
                -> binding.bottomMenu.isVisible = true

                else -> binding.bottomMenu.visibility = View.GONE
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navHostController.navigateUp() || super.onSupportNavigateUp()
    }
}