package com.example.onlinestore.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.onlinestore.R
import com.example.onlinestore.core.base.BaseFragment
import com.example.onlinestore.databinding.FragmentSearchBinding
import com.example.onlinestore.db.base.PModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchFragment : BaseFragment<FragmentSearchBinding>(), ProductsAdapter.ItemClickListener,
    SearchView.OnQueryTextListener{

    override fun getViewBinding() = FragmentSearchBinding.inflate(layoutInflater)
    private val viewModel by viewModel<MainVM>()
    private lateinit var adapter: ProductsAdapter
    private val args: SearchFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSearch()
        setupView()
        initListeners()
    }

    private fun setupView(){

    }

    private fun initListeners(){

    }

    private fun initSearch() {
        binding.searchView.isSubmitButtonEnabled = true
        binding.searchView.setOnQueryTextListener(this@SearchFragment)
    }

    override fun onQueryTextSubmit(query: String): Boolean {
        return true
    }

    override fun onQueryTextChange(query: String): Boolean {
        searchDatabase(query)
        return true
    }

    private fun searchDatabase(query: String) {
        val searchQuery = "%$query%"

        viewModel.searchDatabase(searchQuery).observe(viewLifecycleOwner) { list ->
            list.let {
                adapter.setFilteredList(it)
            }
        }
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onItemClick(item: PModel, index: Int) {
        TODO("Not yet implemented")
    }

    override fun onBackPressed() {
        findNavController().popBackStack()
    }
}