package com.example.onlinestore.ui.splash

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.onlinestore.R

class SplashFragment : Fragment() {
    private val SPLASH_TIME_OUT: Long = 1500

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Handler().postDelayed({

            Navigation.findNavController(view)
                .navigate(R.id.action_fragment_splash_to_mainFragment)
        }, SPLASH_TIME_OUT)
    }

}