package com.example.onlinestore.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.onlinestore.R
import com.example.onlinestore.databinding.ItemCategoryBinding
import java.lang.IllegalStateException

class CategoryAdapter(private val clickListener: ItemClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var items: MutableList<String> = mutableListOf()

    interface ItemClickListener {
        fun onItemClick(item: String, index: Int)
    }

    override fun getItemViewType(position: Int): Int {
        return if (items.isEmpty()) 0 else 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            0 -> EmptyHolder.create(parent)
            1 -> Holder.create(parent, clickListener)
            else -> throw IllegalStateException("Unknown view")
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is Holder -> {
                holder.bind(items[position], position)
            }
        }
    }

    override fun getItemCount(): Int = if (items.isEmpty()) 1 else items.size

    class EmptyHolder private constructor(view: View) : RecyclerView.ViewHolder(view) {
        companion object {
            fun create(parent: ViewGroup): EmptyHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_empty, parent, false)
                return EmptyHolder(view)
            }
        }
    }

    class Holder private constructor(private val binding: ItemCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        lateinit var item: String
        var index: Int = 0

        fun bind(_item: String, position: Int) {
            item = _item
            index = position

            item.apply {
                binding.tvCategoryName.text = item
                when (item) {
                    "electronics" -> { binding.ivImage.background = ActivityCompat.getDrawable(itemView.context, R.drawable.ic_electronics)
                                        binding.tvCategoryName.text = "Электроника"}
                    "jewelery" -> {binding.ivImage.background = ActivityCompat.getDrawable(itemView.context, R.drawable.ic_jawelery_ring)
                        binding.tvCategoryName.text = "Ювелирные изделия"}
                    "men's clothing" -> {binding.ivImage.background = ActivityCompat.getDrawable(itemView.context, R.drawable.ic_man_sweare)
                        binding.tvCategoryName.text = "Мужская одежда"}
                    "women's clothing" -> {binding.ivImage.background = ActivityCompat.getDrawable(itemView.context, R.drawable.ic_woman_sweare)
                        binding.tvCategoryName.text = "Женская одежда"}
                }
            }
        }

        companion object {
            fun create(parent: ViewGroup, clickListener: ItemClickListener): Holder {
                val binding = ItemCategoryBinding
                    .inflate(LayoutInflater.from(parent.context), parent, false)
                return Holder(binding).apply {
                    itemView.setOnClickListener {
                        clickListener.onItemClick(item, index)
                    }
                }
            }
        }
    }
}