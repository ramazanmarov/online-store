package com.example.onlinestore.ui.main

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.widget.SearchView
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.example.onlinestore.R
import com.example.onlinestore.core.base.BaseFragment
import com.example.onlinestore.core.base.ServerErrorHandler
import com.example.onlinestore.databinding.FilterBottomSheetBinding
import com.example.onlinestore.databinding.FragmentProductsBinding
import com.example.onlinestore.db.base.PModel
import com.example.onlinestore.network.base.InternetConnection
import com.example.onlinestore.network.models.ProductModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProductsFragment : BaseFragment<FragmentProductsBinding>(),
    ProductsAdapter.ItemClickListener, SearchView.OnQueryTextListener{
    override fun getViewBinding() = FragmentProductsBinding.inflate(layoutInflater)

    private val viewModel by viewModel<MainVM>()
    private lateinit var adapter: ProductsAdapter
    private val args: ProductsFragmentArgs by navArgs()
    private lateinit var bottomSheetDialog: BottomSheetDialog

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        subsLiveData()
        initListeners()
        setupBottomSheet()
    }

    private fun setupView() = with(binding) {
        rvCategories.layoutManager = GridLayoutManager(requireContext(), 2)
        adapter = ProductsAdapter(this@ProductsFragment)
        rvCategories.adapter = adapter
        viewModel.context = requireContext()
        viewModel.exaptionHandler = ServerErrorHandler(requireContext())
        setTitle()
        getProducts()
    }

    private fun setTitle(){
        when(args.categoryid) {
            "electronics"->{ binding.tvTitle.text = "Электроника"}
            "jewelery"->{ binding.tvTitle.text = "Ювелирные изделия" }
            "men's clothing"->{ binding.tvTitle.text = "Мужская одежда" }
            "women's clothing"->{ binding.tvTitle.text = "Женская одежда" }
        }
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun subsLiveData() {
        viewModel.products.observe(viewLifecycleOwner) {
            adapter.items = it.toMutableList()
            adapter.notifyDataSetChanged()
            binding.swipeRef.isRefreshing = false
            hideDialog()
            viewModel.saveProducts(it)
        }
    }

    private fun initListeners() = with(binding) {
        btnBack.setOnClickListener {
            findNavController().navigateUp()
        }

        btnGoSearch.setOnClickListener {
            val action = ProductsFragmentDirections
                .actionProductsFragmentToSearchFragment("")
            findNavController().navigate(action)
        }

        btnSort.setOnClickListener {
            showBottomSheet()
        }

        binding.swipeRef.setOnRefreshListener {
            viewModel.getProducts(args.categoryid)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun getProducts() {
        if (InternetConnection.isNetAvailable(requireContext())) {
            if (!viewModel.isProductsIsEmpty(args.categoryid)) {
                showDialog()
                viewModel.getProductsFromDB(args.categoryid)
                viewModel.productsFromDB.observe(viewLifecycleOwner) {
                    adapter.items = it.toMutableList()
                    adapter.notifyDataSetChanged()
                    hideDialog()
                }
            } else {
                showDialog()
                viewModel.getProducts(args.categoryid)
            }
        }
    }

    private fun setupBottomSheet() {
        val dialogView = FilterBottomSheetBinding.inflate(layoutInflater)
        bottomSheetDialog = BottomSheetDialog(requireContext(), R.style.BottomSheetDialogTheme)
        bottomSheetDialog.setContentView(dialogView.root)
        dialogView.tvSortAsc.setOnClickListener {}

        dialogView.tvSortDesc.setOnClickListener {}
        dialogView.tvCancel.setOnClickListener {
            bottomSheetDialog.dismiss()
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun showBottomSheet() {
        bottomSheetDialog.show()
    }

    override fun onItemClick(item: PModel, index: Int) {
        val action = ProductsFragmentDirections.actionProductsFragmentToProductDetailsFragment(item)
        findNavController().navigate(action)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return true
    }

    override fun onBackPressed() {
        findNavController().popBackStack()
    }
}