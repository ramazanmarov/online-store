package com.example.onlinestore.ui.main

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.core.base.ResponseHandler
import com.example.onlinestore.core.base.ServerErrorHandler
import com.example.onlinestore.db.base.Categories
import com.example.onlinestore.db.base.OnlineStoreDB
import com.example.onlinestore.db.base.PModel
import com.example.onlinestore.network.models.ProductModel
import com.example.onlinestore.repository.RepositoryApi
import io.reactivex.android.schedulers.AndroidSchedulers

class MainVM(private val repoApi: RepositoryApi) : ViewModel() {

    var db = OnlineStoreDB.INSTANCE
    var categories = MutableLiveData<List<String>>()
    var products = MutableLiveData<List<PModel>>()
    var productDetails = MutableLiveData<ProductModel>()
    var categoriesFromDB: MutableLiveData<List<Categories>> = MutableLiveData()
    var productsFromDB: MutableLiveData<List<PModel>> = MutableLiveData()

    lateinit var exaptionHandler: ServerErrorHandler

    @SuppressLint("StaticFieldLeak")
    lateinit var context: Context

    @SuppressLint("CheckResult")
    fun getCategories() {
        repoApi.getCategories()
            .doFinally {}
            .subscribeWith(object : ResponseHandler<List<String>>(exaptionHandler,
                {
                    categories.postValue(it)
                },
                {

                }
            ) {})
    }

    @SuppressLint("CheckResult")
    fun getCategoriesFromDB() {
        db!!.dao().getCategoriesFromDB()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(categoriesFromDB::setValue)
    }

    fun saveCategories(cats: List<String>) {
        for (c in cats) {
            val cat = Categories(c, c)
            db!!.dao().addCategoriesInDB(cat)
        }
    }

    @Suppress("CheckResult")
    fun getProducts(categoryId: String) {
        repoApi.getProducts(categoryId)
            .doFinally {}
            .subscribeWith(object : ResponseHandler<List<PModel>>(exaptionHandler,
                {
                    products.postValue(it)
                },
                {

                }
            ) {})
    }

    @SuppressLint("CheckResult")
    fun getProductsFromDB(categoryId: String) {
         db!!.dao().getProductsFromDB(categoryId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(productsFromDB::setValue)
    }

    fun saveProducts(products: List<PModel>) {
        for(p in products) {
            db!!.dao().addProductsInDB(p)
        }
    }

    @Suppress("CheckResult")
    fun getProductDetails(id: Int) {
        repoApi.getProductDetails(id)
            .doFinally {}
            .subscribeWith(object : ResponseHandler<ProductModel>(exaptionHandler,
                {
                    productDetails.postValue(it)
                },
                {

                }
            ) {})
    }

//    @SuppressLint("CheckResult")
//    fun getProductDetailsFromDB(id: Int) {
//        db!!.dao().getProductDetailsFromDB(id)
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe(productsFromDB::setValue)
//    }

    fun isCategoriesIsEmpty(): Boolean {
        return db!!.dao().getCategoriesListFromDB().isEmpty()
    }

    fun isProductsIsEmpty(categoryId: String): Boolean {
        return db!!.dao().getProductsListFromDB(categoryId).isEmpty()
    }

    fun isProductDetailsIsEmpty(id: Int): Boolean {
        return db!!.dao().getProductDetailsListFromDB(id).isEmpty()
    }

    fun searchDatabase(searchQuery: String): LiveData<MutableList<PModel>> {
        return db!!.dao().searchDatabase(searchQuery)
    }
}