package com.example.onlinestore.ui.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.onlinestore.core.base.BaseFragment
import com.example.onlinestore.core.base.ServerErrorHandler
import com.example.onlinestore.databinding.FragmentMainBinding
import com.example.onlinestore.network.base.InternetConnection
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : BaseFragment<FragmentMainBinding>(), CategoryAdapter.ItemClickListener {
    private val viewModel by viewModel<MainVM>()
    private lateinit var adapter: CategoryAdapter
    override fun getViewBinding() = FragmentMainBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        subsLiveData()
        initListeners()
    }

    private fun setupView() = with(binding) {
        rvCategories.layoutManager = GridLayoutManager(requireContext(), 2)
        adapter = CategoryAdapter(this@MainFragment)
        rvCategories.adapter = adapter
        viewModel.context = requireContext()
        viewModel.exaptionHandler = ServerErrorHandler(requireContext())
        getCategories()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun subsLiveData() {
        viewModel.categories.observe(viewLifecycleOwner) {
            adapter.items = it.toMutableList()
            adapter.notifyDataSetChanged()
            binding.swipeRef.isRefreshing = false
            viewModel.saveCategories(it)
            hideDialog()
        }
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    private fun initListeners() {
        binding.swipeRef.setOnRefreshListener {
            viewModel.getCategories()
        }

        binding.searchView.setOnSearchClickListener{
        findNavController().navigate(MainFragmentDirections.actionMainFragmentToSearchFragment(binding.searchView.query.toString()))}
    }

    override fun onItemClick(categoryId: String, index: Int) {
        findNavController().navigate(MainFragmentDirections.actionMainFragmentToProductsFragment(categoryId))
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun getCategories() {
        if (InternetConnection.isNetAvailable(requireContext())) {
            if (!viewModel.isCategoriesIsEmpty()) {
                showDialog()
                viewModel.getCategoriesFromDB()
                viewModel.categoriesFromDB.observe(viewLifecycleOwner) { cats ->
                    adapter.items = cats.map { it.name }.toMutableList()
                    adapter.notifyDataSetChanged()
                    hideDialog()
                }
            } else {
                showDialog()
                viewModel.getCategories()
            }
        }
    }

    override fun onBackPressed() {
        if (backPressedTime + doubleBackPressInterval > System.currentTimeMillis()) {
            requireActivity().finish()
        } else {
            Toast.makeText(requireContext(), "Нажмите еще раз для выхода", Toast.LENGTH_SHORT).show()
        }
        backPressedTime = System.currentTimeMillis()
    }
}