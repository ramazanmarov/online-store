package com.example.onlinestore.ui.main

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.onlinestore.R
import com.example.onlinestore.databinding.ItemProductBinding
import com.example.onlinestore.db.base.PModel
import com.example.onlinestore.network.models.ProductModel
import java.lang.IllegalStateException

class ProductsAdapter(private val clickListener: ItemClickListener):
    RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    var items: MutableList<PModel> = mutableListOf()

    interface ItemClickListener{
        fun onItemClick(item: PModel, index: Int)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setFilteredList(filteredList: MutableList<PModel>){
        this.items = filteredList
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return if(items.isEmpty()) 0 else 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when(viewType){
            0 -> EmptyHolder.create(parent)
            1 -> Holder.create(parent, clickListener)
            else -> throw IllegalStateException("Unknown view")
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is Holder -> {
                holder.bind(items[position], position)
            }
        }
    }

    override fun getItemCount(): Int  = if (items.isEmpty()) 1 else items.size

    class EmptyHolder private constructor(view: View) : RecyclerView.ViewHolder(view){
        companion object{
            fun create(parent: ViewGroup): EmptyHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_empty, parent, false)
                return EmptyHolder(view)
            }
        }
    }

    class Holder private constructor(private val binding: ItemProductBinding): RecyclerView.ViewHolder(binding.root){

        lateinit var item: PModel
        var index: Int = 0

        @SuppressLint("SetTextI18n")
        fun bind(_item: PModel, position: Int){
            item = _item
            index = position

            item.apply{
                Glide.with(itemView).load(item.image).into(binding.ivProduct)
                binding.tvPrice.text = "$" + item.price.toString()
                binding.tvDesc.text = item.title
            }
        }

        companion object{
            fun create(parent: ViewGroup, clickListener: ItemClickListener): Holder {
                val binding = ItemProductBinding
                    .inflate(LayoutInflater.from(parent.context), parent, false)
                return Holder(binding).apply {
                    itemView.setOnClickListener {
                        clickListener.onItemClick(item, index)
                    }
                }
            }
        }
    }
}