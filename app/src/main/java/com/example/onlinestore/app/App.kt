package com.example.onlinestore.app

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.example.onlinestore.core.custom.LocaleHelper
import com.example.onlinestore.db.base.OnlineStoreDB
import com.example.onlinestore.di.appModule
import com.example.onlinestore.di.baseModule
import com.example.onlinestore.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: MultiDexApplication() {

    companion object {
        lateinit var instance: App
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        OnlineStoreDB.provideDataBase(instance)

        startKoin {
            androidContext(this@App)
            modules(appModule, networkModule, baseModule)
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(base!!))
    }
}