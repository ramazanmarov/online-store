package com.example.onlinestore.repository

import com.example.onlinestore.db.base.PModel
import com.example.onlinestore.network.api.AppApi
import com.example.onlinestore.network.models.ProductModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RepositoryApi(
    private var appApi: AppApi) {

    fun getCategories(): Observable<List<String>> {
        return appApi.getCategories()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getProducts(param: String): Observable<List<PModel>?> {
        return appApi.getProduct(param)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getProductDetails(param: Int): Observable<ProductModel> {
        return appApi.getProductDetails(param)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

}



